package com.c2t.spring.hbm;

import org.hibernate.Session;
import org.hibernate.impl.SessionFactoryImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppMain 
{
    public static void main( String[] args )
    {
    	ApplicationContext appContext = 
    		new ClassPathXmlApplicationContext("spring/config/BeanLocations-sf.xml");
	
    	SessionFactoryImpl sf = (SessionFactoryImpl)appContext.getBean("sessionFactory1");
    	Session session = sf.openSession();
    	
    	Stock s = new Stock();
    	s.setStockCode("A123");
    	s.setStockName("B123");

    	
    	session.beginTransaction();
    	session.save(s);
    	session.getTransaction().commit();
    	
    }
}
